// Tutaj dodacie zmienne globalne do przechowywania elementów takich jak np. lista czy input do wpisywania nowego todo
let $list;
let $form;
let counter = 0;
let $popupInput;
let $addTodoBtn;
let $myInput;
let modal;
let btnCancel;
let close;
let myTitle;
let curentTodo;
const initialList = ['Dzisiaj robię usuwanie', 'Nakarm psa'];
function main() {  
  prepareDOMElements();
  prepareDOMEvents();
  //prepareInitialList();
  loadTitle();
  getItemsBySerwer();
  showData();
}
function getItemsBySerwer() {
    $list.innerHTML = '';
    axios.get('http://195.181.210.249:3000/todo/')
    .then(function (response) {
      if (response.status === 200) {
        response.data.forEach(todo => {
          addNewElementToList(todo.title, todo.id, todo.extra);
        });
      }
    });
}
function loadTitle() {
  myTitle.classList.add('myTitle'); 
}
function leadingZero(i) {
  return (i < 10)? '0'+i : i;
}
function showData() {
  const currentDate = new Date();
  const days = ["Niedziela", "Poniedziałek", "Wtorek", "Środa", "Czwartek", "Piątek", "Sobota"];
  const textDate = days[currentDate.getDay()] + " - " + leadingZero(currentDate.getDate()) + "." + leadingZero((currentDate.getMonth()+1)) + "." + currentDate.getFullYear();
  const textTime = leadingZero(currentDate.getHours()) + ":" + leadingZero(currentDate.getMinutes()) + ":" + leadingZero(currentDate.getSeconds());
  const dataTodo = document.getElementById('myData');
  const timeTodo = document.getElementById('myTime');
  dataTodo.innerHTML = textDate;
  timeTodo.innerHTML = textTime;
  setTimeout(function() {
    showData()
  }, 1000);
}
function prepareDOMElements() {
  // To będzie idealne miejsce do pobrania naszych elementów z drzewa DOM i zapisanie ich w zmiennych
  myTitle = document.getElementById('myTitle');
  $form = document.getElementById("myForm");
  $list = document.getElementById('list');
  $addTodoBtn = document.getElementById('addTodoBtn');
  $myInput = document.getElementById('myInput');
  $popupInput = document.getElementById('popupInput');
  modal = document.getElementById('myModal');
  btnOk = document.getElementById('btn__done');
  btnCancel = document.getElementById('btn__cancel');
  close = document.getElementById('closePopup');
}
function prepareDOMEvents() {
  // Przygotowanie listenerów
  $list.addEventListener('click', listClickManager);
  //$addTodoBtn.addEventListener('click', addNewTodoToList);
  $form.addEventListener('submit', addNewTodoToListForm);
  btnOk.addEventListener('click',addDataToPopup);
  btnCancel.addEventListener('click',closePopup);
  close.addEventListener('click',closePopup);
}
function prepareInitialList() {
  // Tutaj utworzymy sobie początkowe todosy. Mogą pochodzić np. z tablicy
  initialList.forEach(todo => {
    addNewElementToList(todo);
  });
}
function addNewTodoToListForm(e) {
  e.preventDefault();
  addNewTodoToList();
  }
function addNewElementToList(title, id, extra  /* Title, author, id */) {
  //obsługa dodawanie elementów do listy
  // $list.appendChild(createElement('nowy', 2))
 
  const newElement = createElement(title, id, extra);
  //
  $list.appendChild(newElement);
}
function createElementSerwer(title) {
  axios.post('http://195.181.210.249:3000/todo/',{title:title}).then((response) => {
    if (response.data.status === 0) {
      getItemsBySerwer();
    }
    console.log(response);
  });
}
function createElement(title, id, extra /* Title, author, id */) {
  // Tworzyc reprezentacje DOM elementu return newElement
  // return newElement
  const newElement = document.createElement('li');
  if (extra != null) {
    newElement.classList.add('done');
  }
  newElement.setAttribute('data-id', id);
  const titleElement = document.createElement('span');
  titleElement.innerText = title;
  const titleStyle = document.createElement('div');
  titleStyle.className = 'btn-style';
  const editButton = document.createElement('button');
  editButton.innerText = 'edit';
  editButton.className = 'btn-edit';
  const delButton = document.createElement('button');
  delButton.innerText = 'delete';
  delButton.className = 'btn-delete';
  const okButton = document.createElement('button');
  okButton.innerText = 'ok';
  okButton.className = 'btn-ok';
  newElement.appendChild(titleElement);
  newElement.appendChild(titleStyle);
  titleStyle.appendChild(editButton);
  titleStyle.appendChild(delButton);
  titleStyle.appendChild(okButton);

  return newElement;
}
function addNewTodoToList() {
    if ($myInput.value) {
      //addNewElementToList($myInput.value);
      createElementSerwer($myInput.value);
      $myInput.value='';
    }
  }
function listClickManager(event) {
  // Rozstrzygnięcie co dokładnie zostało kliknięte i wywołanie odpowiedniej funkcji
  // event.target.parentElement.id
  let id = event.target.parentElement.parentElement.dataset.id;
  let extra = '1';
  if (event.target.className === 'btn-edit') {
    let title = document.querySelector('li[data-id="'+id+'"]').querySelector('span').innerText;
    curentTodo = id;
    //console.log(curentTodo)
    editListElement(id, title);
  } else if (event.target.className === 'btn-delete') {
    let dataId = event.target.parentElement.parentElement.dataset.id;
    removeListElement(dataId);
  } else if (event.target.className === 'btn-ok') { 
    markElementAsDone(id, extra);
  }
}
function removeListElement(id) {
  //let liElement = document.querySelector('li[data-id="'+id+'"]');
  axios.delete('http://195.181.210.249:3000/todo/'+ id).then((response) => {
    if (response.data.status === 0) {
      getItemsBySerwer();
    }
  });
}
function editListElement(id, title) {
  // Pobranie informacji na temat zadania
  // Umieść dane w popupie
  let liElement = document.querySelector('li[data-id="'+id+'"]');
  modal.classList.add('modal-show');
  console.log(liElement);
}
function addDataToPopup(title, id) {
  // umieść informacje w odpowiednim miejscu w popupie
  let titleElement = document.querySelector('li[data-id="'+curentTodo+'"]').querySelector('span'); //document.querySelector('li[data-id="'+ id +'"]');
  console.log(titleElement);
  //titleElement.innerText = ;
  updataTodoSerwer($popupInput.value);
  closePopup()
}
function updataTodoSerwer(title, id){ 
  axios.put('http://195.181.210.249:3000/todo/' + curentTodo, {title:title}).then((response) => {
    if (response.data.status === 0) {
      getItemsBySerwer();
    }
  });
}
function closePopup() {
  // Zamknij popup
  modal.classList.remove('modal-show');
}
function markElementAsDone(id, extra) {
  let todoElementOk = document.querySelector('li[data-id="'+id+'"]').querySelector('span');
  todoElementOk.classList.add('done');
  todoElementOk.setAttribute("extra","1");
  axios.put('http://195.181.210.249:3000/todo/' + id,{extra:extra}).then((response) => {
   console.log(response)
  });


    
  }

document.addEventListener('DOMContentLoaded', main);